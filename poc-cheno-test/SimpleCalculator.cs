﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace poc_cheno_test
{
    [TestClass]
    public class SimpleCalculator
    {
        private poc_cheno.SimpleCalculator calculator;

        [TestInitialize]
        public void SetUp()
        {
            this.calculator = new poc_cheno.SimpleCalculator();
        }


        [TestMethod]
        public void Add()
        {
        }

        [TestMethod]
        public void TestAdd()
        {
            float n1 = 1;
            float n2 = 2;
            float expected = 3;
            float actual;

            actual = this.calculator.Add(n1, n2);

            Assert.AreEqual(expected, actual, 0.000);
        }


        [TestMethod]
        public void Multiply()
        {
        }

        [TestMethod]
        public void Divide()
        {
        }
    }
}
