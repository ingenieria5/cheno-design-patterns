﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace repositoryDI
{
    class Program
    {
        static void Main(string[] args)
        {
            string connetionString = null;
            MySqlConnection cnn;
            connetionString = "server=localhost;database=mysqlya;uid=root;pwd=usbw;";
            cnn = new MySqlConnection(connetionString);
            try
            {
                cnn.Open();
                Console.WriteLine("Connection Open ! ");
                var sqlCommand = new MySqlCommand("SELECT * FROM `clientes`LIMIT 50", cnn);
                var sqlAdapter = new MySqlDataAdapter(sqlCommand);
                var table = new DataTable();
                sqlAdapter.Fill(table);
                cnn.Close();
                foreach (DataRow item in table.Rows)
                {
                    Console.WriteLine($"{item["codigo"]} {item["nombre"]}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can not open connection ! ");
            }
        }
    }
}
