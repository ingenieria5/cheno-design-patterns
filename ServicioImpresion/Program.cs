﻿using System;

namespace ServicioImpresion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // Service provider ...
            ServicioImpresion miServicio =
                    new ServicioImpresion(new ServicioEnvio(), new ServicioPDF());

            miServicio.imprimir();

        }


    }
}
