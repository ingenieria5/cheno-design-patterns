﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServicioImpresion
{
    public class ServicioImpresion
    {

        IServicioImpresionInterface servicioA;
        IServicioImpresionInterface servicioB;

        public ServicioImpresion(IServicioImpresionInterface servicioA, IServicioImpresionInterface servicioB)
        {
            this.servicioA = servicioA;
            this.servicioB = servicioB;
        }

        public bool imprimir()
        {
            if (servicioB.enviar() == false)
            {
                Console.WriteLine("El metodo ServicioImpresion.imprimir() en el servicioB.enviar() ha retornado falso ");
                return false;
            }
            if (servicioA.enviar() == false)
            {
                Console.WriteLine("El metodo ServicioImpresion.imprimir() en el servicioA.enviar() ha retornado falso ");
                return false;
            }
            Console.WriteLine("El metodo ServicioImpresion.imprimir() ha operado bien OK");
            return true;
        }
    }
}
