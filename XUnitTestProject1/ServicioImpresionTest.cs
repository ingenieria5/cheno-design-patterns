using ServicioImpresion;
using System;
using System.Threading;
using Telerik.JustMock;
using Xunit;

namespace XUnitTestProject1
{
    public class ServicioImpresionTest
    {
        [Fact]
        public void TestServicioImpresion()
        {

            //ServicioEnvio servicioA = Mock.Create<ServicioImpresion.ServicioEnvio>();
            //ServicioPDF servicioB = Mock.Create<ServicioImpresion.ServicioPDF>();

            var servicioA2 = Mock.Create<IServicioImpresionInterface>();
            var servicioB2 = Mock.Create<IServicioImpresionInterface>();

            Console.WriteLine("Iniciando prueba ...");

            // Interviniendo el comportamiento de los metodos de los objetos mock
            Mock.Arrange(() => servicioA2.enviar()).Returns(true).OccursOnce();
            Mock.Arrange(() => servicioB2.enviar()).Returns(true).OccursOnce();

            // Construccion del SUT
            var SUT2 = new ServicioImpresion.ServicioImpresion(servicioA2, servicioB2);

            // 1ra afirmación
            // Assert - verify that the actual result is equal to the expected.
            Assert.True(SUT2.imprimir());

          

            // Interviniendo el comportamiento de los metodos de los objetos mock
            Mock.Arrange(()=> servicioA2.enviar()).Returns(false).OccursOnce();
            Mock.Arrange(() => servicioB2.enviar()).Returns(true).OccursOnce();            


            // 2ra afirmación
            // Assert - verify that the actual result is equal to the expected.
            Assert.False(SUT2.imprimir());

            // Interviniendo el comportamiento de los metodos de los objetos mock
            Mock.Arrange(() => servicioA2.enviar()).Returns(true).OccursOnce();
            Mock.Arrange(() => servicioB2.enviar()).Returns(false).OccursOnce();


            // 2ra afirmación
            // Assert - verify that the actual result is equal to the expected.
            Assert.False(SUT2.imprimir());

            Console.WriteLine("Terminando prueba ...");
            //Thread.Sleep(20000);
           
        }
    }
}
