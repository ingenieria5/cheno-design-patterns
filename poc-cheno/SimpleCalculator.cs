﻿using System;

namespace poc_cheno
{
    public class SimpleCalculator
    {
        public static void Main() { }

        public float Add(float n1, float n2)
        {
            return n1 + n2;
        }

        public float Subtract(float n1, float n2)
        {
            return n1 - n2;
        }

        public float Multiply(float n1, float n2)
        {
            return n1 * n2;
        }

        public float Divide(float n1, float n2)
        {
            if ((int)n2 == 0)
            {
                throw new ArithmeticException("Cannot divide by zero.");
            }

            return n1 / n2;
        }
    }
}
